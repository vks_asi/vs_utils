from AsiUtilities.data_file_io import read_mp
import csv

if __name__=='__main__':
    fn = '/home/asi/git/vs_utils/csv/sband.csv'

    of = '/home/asi/git/vs_utils/csv/sband_rs.csv'

    data = []
    with open (fn, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(row)

    new_list = []
    for d in data:
        try:
            print(d['FILE'])
            data, nav, rs, _ =  read_mp(d['FILE'])
            cf = rs['frequency'][0]
            d['cf'] = cf
            new_list.append(d)
            print(d)
        except KeyboardInterrupt:
            break

    with open(of,'w') as outfo:
        writer = csv.DictWriter(outfo, fieldnames=new_list[0].keys())
        writer.writeheader()
        for x in new_list:
            writer.writerow(rowdict=x)
        outfo.close()