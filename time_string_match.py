import re
import datetime
import time

from datetime import datetime
from pytz import timezone


file = '/run/media/asi/ASI_EXT1/CALCOFI_9380_264/CALCOFI_9380_264-nb-2018-10-16_11-30-30-106606.dat'

match_day = re.search('-\d{2}_', file)


day = match_day.group()
d = day[1:3]


match_hour_min_sec = re.search('_\d{2}-\d{2}-\d{2}', file)

hms = match_hour_min_sec.group()
h = hms[1:3]
m = hms[4:6]
s = hms[7:]

year = '2018'
month = '10'
#print('Day {0}, Hour {1}, Minute {2}, Second {3}'.format(d,h,m,s))

pdt_string = year +'-'+month+'-'+d+'_'+h+'-'+m+'-'+s

#print(pdt_string)

datetime_obj_naive = datetime.strptime(pdt_string, "%Y-%m-%d_%H-%M-%S")

# Right way!
datetime_obj_pacific = timezone('US/Pacific').localize(datetime_obj_naive)

datetime_obj_utc = datetime_obj_pacific.replace(tzinfo=timezone('UTC'))

dt_obj_utc_psx = datetime_obj_utc.timestamp()

print(datetime_obj_pacific)
print(datetime_obj_utc)
print(dt_obj_utc_psx)



