import os
import glob

import matplotlib.pyplot as plt
import numpy as np
import csv
import time
import os
from SignalProcessing.plot_overload import *
from AsiUtilities.data_file_io import read_mp
from scipy.signal import decimate



def get_recording_data(input_file):
    """
    Get data from an input_file created using savez or AsiUtilities.data_file_io read_mp
    :param input_file: File to load
    :return: data, nav, rs,
    """
    if '.npz' in input_file:
        loaded = np.load(input_file)
        data = loaded['tdd']
        nav = loaded['nav_data']
        rs = loaded['radio_state']
        rs = rs[()]
        nav = nav[()]
    elif '.dat' in input_file:
        data, nav, rs, _ = read_mp(input_file)
    else:
        data = None
        nav = None
        rs = None
    return data, nav, rs




def plot_phase():


    test_data_path = '/run/media/asi/ASI_EXT2/*AIS*.npz'
    file_list = glob.glob(test_data_path)

    loaded = np.load(file_list[0])
    tdata = loaded['tdd']
    tnav = loaded['nav_data']
    tradio = loaded['radio_state']

    tradio = tradio[()]
    tnav = tnav[()]

    bw = tradio['bandwidth']
    freq = tradio['frequency']


    for x in range(4, 5, 1):
        cnt = 0
        while cnt < 1000:
            plt.polar([0, np.angle(tdata[x][cnt])], [0, abs(tdata[x][cnt])], marker='x', linewidth=0.1)
            cnt += 1


    plt.show()

def plot_spectogram():

    test_data_path = '/run/media/asi/ASI_EXT2/CALCOFI_8355_12HR2018-10-24_16-4*AIS*.npz'
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)
    for file in file_list:
        loaded = np.load(file)
        tdata = loaded['tdd']
        tnav = loaded['nav_data']
        tradio = loaded['radio_state']

        tradio = tradio[()]
        tnav = tnav[()]

        bw = tradio['bandwidth']
        freq = tradio['frequency']

        for x in range (4, 5, 1):
            plt.specgram(tdata[x], NFFT=2048 ,Fs=12.8e6, Fc=162000000)
        print('Plotting : {}'.format(file[24:]))
        plt.title(file[24:])
        plt.show()
        plt.gcf().clear()

def plot_psd():

    test_data_path = '/run/media/asi/ASI_EXT2/*AIS*.npz'
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)
    for file in file_list:
        loaded = np.load(file)
        tdata = loaded['tdd']
        tnav = loaded['nav_data']
        tradio = loaded['radio_state']

        tradio = tradio[()]
        tnav = tnav[()]

        bw = tradio['bandwidth']
        cf = tradio['frequency'][0]

        fdd = np.fft.fft(tdata[4])
        spectrum = np.fft.fftshift(fdd)
        n = spectrum.shape[0]

        spectrum_freq = np.arange(-n / 2, n / 2) * tradio['data_rate'] / n + cf

        plot(spectrum_freq, 20*np.log10(abs(spectrum)))
        print('Plotting : {}'.format(file[24:]))
        plt.title(file[24:])
        plt.show()

def plot_psd_16(infile, dir, channel):

    test_data_path = dir+infile
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)
    for file in file_list:
        loaded = np.load(file)
        tdata = loaded['tdd']
        tnav = loaded['nav_data']
        tradio = loaded['radio_state']

        tradio = tradio[()]
        tnav = tnav[()]

        bw = tradio['bandwidth']
        cf = tradio['frequency'][0]

        # conv_int16 = simple_convert(tdata[4], array_length=len(tdata[4]))

        fdd = np.fft.fft(tdata[channel])
        spectrum = np.fft.fftshift(fdd)
        n = spectrum.shape[0]

        spectrum_freq = np.arange(-n / 2, n / 2) * tradio['data_rate'] / n + cf
        print(len(spectrum_freq), len(spectrum))

        spectrum = spectrum[17480000:23480000]
        spectrum_freq = spectrum_freq[17480000:23480000]
        print(spectrum_freq[0::1000])

        plot(spectrum_freq, 20*np.log10(abs(spectrum)))
        print('Plotting : {}'.format(file[24:]))
        plt.title(infile)
        ais1 = 1.61975e8
        ais2 = 1.62025e8
        tfd_bw = 25000

        # Plot AIS Channel 2 BWs
        plt.vlines(ais1, 0, 125, colors='r', linestyles='--', linewidth=0.3)
        plt.vlines(ais1+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais1-480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais1 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
        plt.vlines(ais1 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)


        # Plot AIS Channel 2 BWs
        plt.vlines(ais2, 0, 125, colors='r', linestyles='--', linewidth=0.3)
        plt.vlines(ais2+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais2 -480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais2 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
        plt.vlines(ais2 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
        plt.show()

def plot_psd_time_segments(infile, dir, channel,save_plot, time_ms, post_process):
    test_data_path = dir + infile
    if '.csv' in infile:
        file_list = []
        with open(test_data_path, 'r', newline='') as cvsfile:
            has_header = csv.Sniffer().has_header(cvsfile.readline(1024))
            cvsfile.seek(0)
            csv_reader = csv.reader(cvsfile)
            if has_header:
                next(csv_reader)
            for row in csv_reader:
                file_list.append(row[0])
    else:
        file_list = glob.glob(test_data_path)
        file_list.sort(key=os.path.getmtime)
    for file_entry in file_list:
        if '.npz' in file_entry:
            loaded = np.load(file_entry)
            tdata = loaded['tdd']
            tnav = loaded['nav_data']
            tradio = loaded['radio_state']
            tradio = tradio[()]
            tnav = tnav[()]
        elif '.dat' in file_entry:
            tdata, tnav, tradio, _ = read_mp(file_entry)

        bw = tradio['bandwidth']
        cf = tradio['frequency'][0]
        ct = tradio['collect_time']
        sample_rate = tradio['data_rate']

        ms_frac = time_ms/1000

        if post_process is True:
            plot_data = shift_decimate(tdata[channel])
            seg_sr = ms_frac * (sample_rate/12)

        else:
            plot_data = tdata[channel]
            seg_sr = ms_frac * sample_rate

        segments = (plot_data.size//(seg_sr))

        segments = int(segments)

        if cf in [162500000, 162500000.0]:
            cf = 162000000
            try:
                tdd_split = np.split(plot_data, segments)
            except ValueError:
                print("Value Error, re-calculating split size.")
                mod = int(plot_data.size % segments)
                tdd_split = np.split(plot_data[mod:], segments)

            seg_cnt = 1
            for segment in tdd_split:

                fdd = np.fft.fft(segment)
                spectrum = np.fft.fftshift(fdd)
                n = spectrum.shape[0]

                if post_process is True:
                    freq_vec = np.arange(-n // 2, n // 2) * (sample_rate/ seg_sr) + 162e6
                else:
                    #freq_vec = np.arange(-n // 2, n // 2) * (sample_rate/ seg_sr) + cf
                    freq_vec = np.arange(-n // 2, n // 2) * (sample_rate / n) + cf

                plot(freq_vec, 20*np.log10(abs(spectrum)))
                print('Plotting : {0}, Segment {1}'.format(file_entry, seg_cnt))
                plt.title('{0}, Segment {1}'.format(file_entry,seg_cnt))
                if post_process is True:
                    plt.axis([161.9e6, 162.1e6, -20, 120])
                else:
                    plt.axis([161.9e6,162.1e6,60,120])

                ais1 = 1.61975e8
                ais2 = 1.62025e8
                tfd_bw = 25000

                # Plot AIS Channel 2 BWs
                plt.vlines(ais1, 0, 125, colors='r', linestyles='--', linewidth=0.3)
                plt.vlines(ais1+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais1-480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais1 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
                plt.vlines(ais1 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)


                # Plot AIS Channel 2 BWs
                plt.vlines(ais2, 0, 125, colors='r', linestyles='--', linewidth=0.3)
                plt.vlines(ais2+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais2 -480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais2 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
                plt.vlines(ais2 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
                plt.grid(True)
                hold(False)

                f_path, f_name = os.path.split(file_entry)
                save_dir = '/home/asi/Figures/'
                if post_process is True:
                    save_fn = '{0}_chan{1}_segment{2}_post_processed.png'.format(f_name, channel, seg_cnt)
                else:
                    save_fn = '{0}_chan{1}_segment{2}.png'.format(f_name,channel,seg_cnt)
                font = {'family': 'FreeSans',
                        'color': 'black',
                        'weight': 'normal',
                        'size': 10,
                        }

                plt.title(save_fn, fontdict=font)

                plt.show(block=False)



                if save_plot is True:
                    plt.savefig(save_dir+save_fn,format='png')
                    print('Saving {0}{1}'.format(save_dir, save_fn))
                plt.pause(0.75)
                seg_cnt +=1
        else:
            print('{0} tune frequency was {1}'.format(file_entry,cf))
    return



def shift_decimate(signal, fs=12.8e6, freq_shift=0.5e6, decimation_factor=12):
    """
    Shift the signal, then decimate
    :param signal: Input Signal (one channel) to shift and decimate
    :param fs: Sample Rate
    :param freq_shift: To shift the signal to the left by 500 kHz, freq_shift=0.5e6
    :param decimation_factor: decimation factor for scipy.signal decimate
    :return: signal
    """
    if freq_shift != 0:
        out_signal = signal*(np.exp(1j * 2 * np.pi * freq_shift * np.arange(signal.size[-1]) / fs))
    else:
        out_signal = signal.copy()
    out_signal = decimate(out_signal, decimation_factor)
    return out_signal

def convert(chandata, array_length, byte_flag):
    """
    Convert 32 bit complex values to 16 bit integer alternating I/Q Samples
    :param chandata: Data to convert
    :param array_length: Length of array to store converted values
    :param byte_flag: If True, output data as bytes
    :return: Byte array of 16 bit integer alternating I/Q Samples
    """
    # Normalize data
    l1 = abs(chandata.real).max()
    l2 = abs(chandata.imag).max()
    l = max(l1, l2)

    print('Input Max is {}'.format(l))
    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = np.int16(chandata.real * (((1 << 15)-1) / l / np.sqrt(2)))
    a[1::2] = np.int16(chandata.imag * (((1 << 15)-1) / l / np.sqrt(2)))
    #x = (chandata * ((1 << 15) / l))
    # Round x
    # x = np.round(x, decimals=0)
    # Convert to int16
    # a = np.zeros(2 * array_length, np.int16)
    # a[0::2] = x.real
    # a[1::2] = x.imag

    mins = len(np.where(a==-32768))
    maxs = len(np.where(a==32767))
    print('Normailized Max a is {0}, Min a is {1}'.format(max(a), min(a)))
    print('Num normalized max vals : {0}, Num min vals : {1}'.format(maxs,mins))

    #print('A Size : {}'.format(a.size))
    #npad = ((512, 0))
    #a = np.pad(a,npad, mode='constant', constant_values=0)
    #print('a size {}'.format(a.size))
    #print('a : {}'.format(a[256:512]))
    print('Chunk Size : {} '.format((a.size)))

    # This gives you alternating I/Q 16 bit integers, just like the data stream from the iVeia radio.

    # Now, we can convert to a byte stream with
    #a.byteswap()
    if byte_flag is True:
        return a.tobytes('C')
    elif byte_flag is False:
        return a

def simple_convert(chandata, array_length):

    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = chandata.real.astype(np.int16)
    a[1::2] = chandata.imag.astype(np.int16)
    return a


if __name__=='__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", help="Find .npz or .dat files that match this string. Can read .dat or "
                                         ".npz directory and filenames from a csv", type=str, default='*AIS*.npz')
    parser.add_argument("--dir", help="Directory to search", type=str, default='/run/media/asi/ASI_EXT2/')
    parser.add_argument("--channel", help="Channel to plot", type=int, default=4)
    parser.add_argument("--time_ms", help="Time in ms to FFT", type=int, default=100)
    parser.add_argument("--save_plot", help="Save Plot Output", type=bool, default=False)
    parser.add_argument('--post_process', help='Shift signal, decimate for AIS processing', type=bool, default=False)
    args = parser.parse_args()

    #plot_phase()
    #plot_spectogram()
    #plot_psd()

    # args.infile='ais.csv'
    # args.dir='/home/asi/git/vs_utils/'
    # args.time_ms=100


    print('Searching to build file list {0}{1}'.format(args.dir,args.infile))

    if args.time_ms == 0:
        plot_psd_16(infile=args.infile, dir=args.dir, channel=args.channel)
    elif args.time_ms >= 1:
        plot_psd_time_segments(infile=args.infile, dir=args.dir, channel=args.channel, time_ms=args.time_ms,
                               save_plot=args.save_plot, post_process=args.post_process)
    else:
        print('Invalid argument for --segments')
