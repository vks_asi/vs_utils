from AsiUtilities.data_file_io import read_mp
import glob
import os
import csv

def find_cf():
    dir = '/run/media/asi/ASI_EXT1/*/'
    fn = '*.dat'

    test_data_path = dir + fn
    file_list = glob.glob(test_data_path, recursive=True)
    file_list.sort(key=os.path.getmtime)
    print(file_list[0:3])
    ais_dats = []
    sband_dats = []
    for file in file_list:
        data, nav, radio, _ = read_mp(file)
        try:
            cf = radio['frequency'][0]
        except TypeError:
            continue
        if cf in [162500000, 162500000.0]:
            ais_dats.append(file)
            print('{} is an AIS Recording'.format(file))
        else:
            sband_dats.append(file)
            print('{} is an S-band Recording'.format(file))

    return(ais_dats,sband_dats)

def write_csv(list, input_csv, fields):
    with open(input_csv, 'w') as csvfile:
        fieldnames = fields
        writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(fieldnames)
        for entry in list:
            writer.writerow([entry])
    return

if __name__=='__main__':

    ais_csv = '/home/asi/git/vs_utils/ais.csv'
    sband_csv = '/home/asi/git/vs_utils/sband.csv'
    csv_fields = ['FILE']

    ais_files, sband_files = find_cf()
    for elem in ais_files:
        print(elem)

    write_csv(ais_files, ais_csv, csv_fields)
    write_csv(sband_files, sband_csv, csv_fields)