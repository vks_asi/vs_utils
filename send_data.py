#! /usr/bin/python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import socket
import os
import numpy as np
import glob
import math as mth
from AsiUtilities.data_file_io import read_mp
from AsiNavigation.Navigation import Navigation
#from RadioControl.Ndr308Interface import Ndr308Interface
from RadioControl.AsiNdr308InterfaceApi import AsiNdr308InterfaceApi
from scipy.signal import decimate
from time import sleep

DEST_IP = '127.0.0.1'
DEST_PORT = 50010

BIND_IP = '127.0.0.1'
BIND_PORT = 50000
DATA_PATH = '/mnt/data0/data/'
LOG_PATH = '/mnt/data0/log/'
BASENAME = 'AIS_TEST'

SPLIT_SIZE = 1024
#SPLIT_SIZE = 512
#SPLIT_SIZE = 256
BYTE_SPLIT_SIZE = 4096
WAIT_TIME = .001

def send_data_from_radio():
    nav = Navigation()
    radio = AsiNdr308InterfaceApi()
    radio.configure_wideband_ddc_group(data_rate=radio.wideband_filter_index['12.8 Msps'])
    print('Radio Sample Rate Set')
    radio.set_collection_time(0.65536)
    print('Radio Collection Time Set')
    radio.set_frequency(162e6)
    print('Radio Frequency Set')
    radio.get_iq_data()
    ndata = nav.get_nav()
    tdd = radio.get_time_domain_data()
    # print(radio.data[4].size)
    # print(radio.data[4].itemsize)
    # print(type(radio.data[4]))
    # print(radio.data[4].size*radio.data[4].itemsize)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket Created')
    s.bind((BIND_IP, 0))
    print('Socket Bound')
    pc = 0
    stop = False
    try:
        while stop is False:
            print('{}'.format(radio.radio_parameters))
            print('Getting I/Q Data')
            radio.get_iq_data()
            for x in range(4,8,1):
                print('Sending Channel : {}'.format(x))
                num_split = radio.data[x].size/SPLIT_SIZE
                split_data = np.split(radio.data[x], num_split)
                for dd in split_data:
                    dd = convert(dd, SPLIT_SIZE)                            # Convert to single type complex representation,
                                                                 # Redhawk AIS implemenation expects 2 bytes, not four
                    s.sendto(dd,(DEST_IP, DEST_PORT))
                    pc +=1
            #write_mp(deepcopy(tdd), deepcopy(ndata), radio_state=deepcopy(radio.get_radio_state()),
                     #data_type=DataType.NarrowBand, path=DATA_PATH, FILENAME_GPS_TIME=False,
                     #auto_basename=BASENAME)
            print('{} Packets Sent'.format(pc))
    except KeyboardInterrupt:
        stop = True
        print('Exiting')
    s.close()
    print('Socket Closed')

    return




def send_data_from_file():
    """
    Send data to a socket from recording
    :return: None
    """
    # TODO : Select a closer ais frequency from radio tune list
    stop = False
    test_data_path = '/home/asi/test_data/*AIS*.npz'
    #test_data_path = '/run/media/asi/ASI_EXT1/CALCOFI_SCCOOS_underway/*.dat'
    test_data_path = '/run/media/asi/ASI_EXT2/*AIS*.npz'
    file_list = glob.glob(test_data_path)
    print(file_list)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket Created')
    s.bind((BIND_IP, 0))
    print('Socket Bound')

    try:
        while stop is False:
            for file in file_list:
                if '.dat' in test_data_path:
                    tdata, tnav, tradio, _ = read_mp(file)
                    pc = 0
                    print('Transmitting I/Q Data from {}'.format(file))
                    for x in range(4,8,1):
                        print('Sending Channel : {}'.format(x))
                        print(tdata[x].size)
                        num_split = tdata[x].size/SPLIT_SIZE
                        split_data = np.split(tdata[x], num_split)
                        for dd in split_data:
                            dd = convert(dd, SPLIT_SIZE)
                            s.sendto(dd,(DEST_IP, DEST_PORT))
                            pc +=1
                    print('{0} Packets Sent from file {1}'.format(pc,file))
                elif '.npz' in test_data_path:
                    loaded = np.load(file)
                    tdata = loaded['tdd']
                    tnav = loaded['nav_data']
                    tradio = loaded['radio_state']
                    pc = 0
                    print('Transmitting I/Q Data from {}'.format(file))
                    for x in range(4, 8, 1):
                        print('Sending Channel : {}'.format(x))
                        print(tdata[x].size)
                        num_split = tdata[x].size / SPLIT_SIZE
                        split_data = np.split(tdata[x], num_split)
                        for dd in split_data:
                            dd = convert(dd, SPLIT_SIZE)
                            s.sendto(dd, (DEST_IP, DEST_PORT))
                            pc += 1
                    print('{0} Packets Sent from file {1}'.format(pc, file))
    except KeyboardInterrupt:
        stop = True
        s.close()
        print('Socket Closed')
    return


def send_data_convert_split(infile, directory,loops, start_channel, end_channel, debug, resample):
    """
    Convert a data file to bytes, and send through a UDP socket. Performs conversion on entire file prior to transmit
    :param infile: Directory and file string to search for and play back
    :param start_channel: If file contains multiple channels, use this to specify which channel to start at
    :param end_channel: If file contains multiple channels, use this to specify which channel to end at, non-inclusive
    :param loops: Number of loops
    :return: 1 on Success, 0 on Failure
    """
    num_loops = 0

    stop = False
    test_data_path = directory+infile
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket Created')
    s.bind((BIND_IP, 0))
    print('Socket Bound')

    try:
        while stop is False:
            fc = 0
            for file in file_list:
                if '.dat' in test_data_path:
                    tdata, tnav, tradio, _ = read_mp(file)
                elif '.npz' in test_data_path:
                    loaded = np.load(file)
                    tdata = loaded['tdd']
                    tnav = loaded['nav_data']
                    tradio = loaded['radio_state']
                else:
                    print('Invalid input file.')
                    stop = True
                print('Radio State: {}'.format(tradio))
                pc = 0
                print('Transmitting I/Q Data from {}'.format(file))
                for x in range(start_channel,end_channel,1):
                    print('Sending Channel : {}'.format(x))
                    if debug is True:
                        print(tdata[x].size)

                    if resample is True:
                        print('Resamplings')
                        rs_data = shift_decimate(tdata[x], freq_shift=0, decimation_factor=10)
                        conv_data = convert(rs_data, rs_data.size, byte_flag=True)
                    else:
                        conv_data = convert(tdata[x], tdata[x].size, byte_flag=True)
                    dx = np.arange(0, len(conv_data), BYTE_SPLIT_SIZE)
                    xx = 0
                    if debug is True:
                        print(len(conv_data))
                    while xx < len(dx)-1:
                        send_this = conv_data[dx[xx]:dx[xx+1]]
                        s.sendto(send_this,(DEST_IP, DEST_PORT))
                        sleep(WAIT_TIME)
                        xx += 1
                        pc += 1
                fc += 1
                print('{0} Packets Sent from file {1}, File {2} of {3}, Loop {4} of {5}'
                      .format(pc, file[24:],fc, len(file_list), num_loops,loops))
                print('--------------------------------------')
            num_loops += 1
            if num_loops == loops:
                print('Playback completed, Stopping......')
                stop = True
    except KeyboardInterrupt:
        stop = True
        s.close()
        print('Socket Closed')
    s.close()
    print('Socket Closed')
    return

def shift_decimate(signal, fs=12.8e6, freq_shift=0.5e6, decimation_factor=12):
    """
    Shift the signal, then decimate
    :param signal: Input Signal (one channel) to shift and decimate
    :param fs: Sample Rate
    :param freq_shift: To shift the signal to the left by 500 kHz, freq_shift=0.5e6
    :param decimation_factor: decimation factor for scipy.signal decimate
    :return: signal
    """
    if freq_shift != 0:
        out_signal = signal*(np.exp(1j * 2 * np.pi * freq_shift * np.arange(signal.size[-1]) / fs))
    else:
        out_signal = signal.copy()
    out_signal = decimate(out_signal, decimation_factor)
    return out_signal

def recv_data():
    HOST = '127.0.0.1'
    PORT = 32191
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(1024)

    print('Received', repr(data))

def convert(chandata, array_length, byte_flag):
    """
    Convert 32 bit complex values to 16 bit integer alternating I/Q Samples
    :param chandata: Data to convert
    :param array_length: Length of array to store converted values
    :param byte_flag: If True, output data as bytes
    :return: Byte array of 16 bit integer alternating I/Q Samples
    """
    # Normalize data
    l1 = abs(chandata.real).max()
    l2 = abs(chandata.imag).max()
    l = max(l1, l2)

    print('Input Max is {}'.format(l))
    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = np.int16(chandata.real * (((1 << 15)-1) / l / np.sqrt(2)))
    a[1::2] = np.int16(chandata.imag * (((1 << 15)-1) / l / np.sqrt(2)))
    #x = (chandata * ((1 << 15) / l))
    # Round x
    # x = np.round(x, decimals=0)
    # Convert to int16
    # a = np.zeros(2 * array_length, np.int16)
    # a[0::2] = x.real
    # a[1::2] = x.imag

    mins = len(np.where(a==-32768))
    maxs = len(np.where(a==32767))
    print('Normailized Max a is {0}, Min a is {1}'.format(max(a), min(a)))
    print('Num normalized max vals : {0}, Num min vals : {1}'.format(maxs,mins))

    #print('A Size : {}'.format(a.size))
    #npad = ((512, 0))
    #a = np.pad(a,npad, mode='constant', constant_values=0)
    #print('a size {}'.format(a.size))
    #print('a : {}'.format(a[256:512]))
    print('Chunk Size : {} '.format((a.size)))

    # This gives you alternating I/Q 16 bit integers, just like the data stream from the iVeia radio.

    # Now, we can convert to a byte stream with
    #a.byteswap()
    if byte_flag is True:
        return a.tobytes('C')
    elif byte_flag is False:
        return a

def simple_convert(chandata, array_length):

    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = chandata.real.astype(np.int16)
    a[1::2] = chandata.imag.astype(np.int16)
    return a.tobytes('C')

def alternate_IQ_floats(chandata, array_length):
    a = np.zeros(2 * array_length, np.float32)
    a[0::2] = chandata.real.astype(np.float32)
    a[1::2] = chandata.imag.astype(np.float32)
    return a.tobytes('C')

def alternateIQ_float32(chandata, array_length):
    """
    Convert 32 bit complex values to 32 bit floats alternating I/Q Samples
    :return: Byte array of 32 bit floats alternating I/Q Samples
    """

    # Convert to alternating iq
    a = np.zeros(2 * array_length, np.float32)
    a[0::2] = chandata.real
    a[1::2] = chandata.imag
    print('Chunk Size : {} Bytes'.format((a.size)))

    # This gives you alternating I/Q 16 bit floats.

    # Now, we can convert to a byte stream with
    #a.byteswap()
    return a.tobytes()

def send_data_from_file_TCP():
    stop = False
    #test_data_path = '/home/asi/test_data/*AIS*.npz'
    #test_data_path = '/run/media/asi/ASI_EXT1/CALCOFI_SCCOOS_underway/*.dat'
    #test_data_path = '/run/media/asi/ASI_EXT2/*AIS*.npz'
    test_data_path = '/run/media/asi/ASI_EXT2/*AIS*.npz'
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)
    print(file_list)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket Created')
    server_address = ('localhost', BIND_PORT)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(server_address)
    s.listen(5)
    print('Starting on {}:{}'.format(*server_address))
    conn, addr = s.accept()
    print('Connected to {}'.format(addr))

    pc = 0
    fc = 0
    try:
        for file in file_list:
            if '.npz' in test_data_path:
                loaded = np.load(file)
                fc += 1
                tdata = loaded['tdd']
                tnav = loaded['nav_data']
                tradio = loaded['radio_state']
                pc = 0
                print('Transmitting I/Q Data from {}'.format(file))
                for x in range(4, 5, 1):
                    print('Sending Channel : {}'.format(x))
                    print(tdata[x].size)
                    # conv_data = convert(tdata[x], tdata[x].size)
                    conv_data = alternate_IQ_floats(tdata[x], tdata[x].size)
                    dx = np.arange(0, len(conv_data), BYTE_SPLIT_SIZE)
                    xx = 0
                    print(len(conv_data))
                    while xx < len(dx) - 1:
                        send_this = conv_data[dx[xx]:dx[xx + 1]]
                        xx += 1
                        pc += 1
                        print('{0} Packets Sent from file {1}, File {2} of {3}'.format(pc, file[24:], fc, len(file_list)))
                        conn.send(send_this)

                print('{} Packets Sent'.format(pc))
    except KeyboardInterrupt:
        print('Exiting')
        s.shutdown(1)
        s.close()
        print('Socket Closed')

    return


def add_args(config):
    from argparse import ArgumentParser
    import os
    """Adds commandline arguments and formatted Help"""
    parser = ArgumentParser(description=os.path.basename(__file__))
    parser.add_argument('--infile',
                        help="Input File to search (default={})".format(config['infile']),
                        type=str,
                        default=config['infile'])
    parser.add_argument('--directory',
                        help="Directory to Search (default={})".format(config['directory']),
                        type=str,
                        default=config['directory'])
    parser.add_argument('--loops',
                        help="Loops to run (default={})".format(config['loops']),
                        type=int,
                        default=config['loops'])
    parser.add_argument('--start_channel',
                        help="Start Channel in data file to playback(default={})".format(config['start_channel']),
                        type=int,
                        default=config['start_channel'])
    parser.add_argument('--end_channel',
                        help="End Channel in data file to playback(default={})".format(config['end_channel']),
                        type=int,
                        default=config['end_channel'])
    parser.add_argument('--debug',
                        help="Enable debug output(default={})".format(config['debug']),
                        type=bool,
                        default=config['debug'])
    parser.add_argument('--resample', help="Shift and decimate signal (default={})".format(config['resample'])
                        , type=bool, default=config['resample'])
    args = parser.parse_args()
    for option in config.keys():
        config[option] = getattr(args, option, config[option])
    return config


if __name__=='__main__':
    from os.path import basename
    from setproctitle import setproctitle
    setproctitle(basename(__file__))

    application_config = dict(
        infile='TEST2018-10-24_15-46-19-997333test_ping_AIS_162000000.0.npz',
        directory='/run/media/asi/ASI_EXT2/',
        loops=1,
        start_channel=4,
        end_channel=5,
        debug=False,
        resample=False,
    )

    application_config = add_args(application_config)
    print(application_config)
    print('--------------------------------------')

    #send_data_from_radio()
    #send_data_from_file()
    send_data_convert_split(infile=application_config['infile'], loops=application_config['loops'],
                            directory=application_config['directory'],
                            start_channel=application_config['start_channel'],
                            end_channel=application_config['end_channel'],
                            debug=application_config['debug'], resample=application_config['resample'])
    #send_data_from_file_TCP()
