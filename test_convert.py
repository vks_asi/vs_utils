import os
import glob

import matplotlib.pyplot as plt
import numpy as np
import csv
import time
import os
from SignalProcessing.plot_overload import *
from AsiUtilities.data_file_io import read_mp
from wait_key_press import WaitKeyPress


def plot_psd_16(infile, dir, channel):

    test_data_path = dir+infile
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)
    for file in file_list:
        loaded = np.load(file)
        tdata = loaded['tdd']
        tnav = loaded['nav_data']
        tradio = loaded['radio_state']

        tradio = tradio[()]
        tnav = tnav[()]

        bw = tradio['bandwidth']
        cf = tradio['frequency'][0]

        # conv_int16 = simple_convert(tdata[4], array_length=len(tdata[4]))

        fdd = np.fft.fft(tdata[channel])
        spectrum = np.fft.fftshift(fdd)
        n = spectrum.shape[0]

        spectrum_freq = np.arange(-n / 2, n / 2) * tradio['data_rate'] / n + cf
        print(len(spectrum_freq), len(spectrum))

        spectrum = spectrum[17480000:23480000]
        spectrum_freq = spectrum_freq[17480000:23480000]
        print(spectrum_freq[0::1000])

        plot(spectrum_freq, 20*np.log10(abs(spectrum)))
        print('Plotting : {}'.format(file[24:]))
        plt.title(infile)
        ais1 = 1.61975e8
        ais2 = 1.62025e8
        tfd_bw = 25000

        # Plot AIS Channel 2 BWs
        plt.vlines(ais1, 0, 125, colors='r', linestyles='--', linewidth=0.3)
        plt.vlines(ais1+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais1-480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais1 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
        plt.vlines(ais1 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)


        # Plot AIS Channel 2 BWs
        plt.vlines(ais2, 0, 125, colors='r', linestyles='--', linewidth=0.3)
        plt.vlines(ais2+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais2 -480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
        plt.vlines(ais2 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
        plt.vlines(ais2 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
        plt.show()
        return

def plot_psd_time_segments(infile, dir, channel,save_plot, time_ms, conv_bytes):

    test_data_path = dir+infile
    file_list = glob.glob(test_data_path)
    file_list.sort(key=os.path.getmtime)
    for file_entry in file_list:
        if '.npz' in file_entry:
            loaded = np.load(file_entry)
            tdata = loaded['tdd']
            tnav = loaded['nav_data']
            tradio = loaded['radio_state']
            tradio = tradio[()]
            tnav = tnav[()]
        elif '.dat' in file_entry:
            tdata, tnav, tradio, _ = read_mp(file_entry)

        bw = tradio['bandwidth']
        cf = tradio['frequency'][0]
        ct = tradio['collect_time']
        sample_rate = tradio['data_rate']

        ms_frac = time_ms/1000

        seg_sr = ms_frac * sample_rate

        segments = (tdata[channel].size//(seg_sr))
        segments = int(segments)

        if cf in [162500000, 162500000.0]:
            try:
                tdd_split = np.split(tdata[channel], segments)
            except ValueError:
                print("Value Error, re-calculating split size.")
                mod = int(tdata[channel].size % segments)
                tdd_split = np.split(tdata[channel][mod:], segments)

            seg_cnt = 1
            for segment in tdd_split:

                fdd = np.fft.fft(segment)
                spectrum = np.fft.fftshift(fdd)
                n = spectrum.shape[0]

                freq_vec = np.arange(-n // 2, n // 2) * (sample_rate / seg_sr) + cf

                plot(freq_vec, 20*np.log10(abs(spectrum)))
                print('Plotting : {0}, Segment {1}'.format(file_entry, seg_cnt))
                plt.title('{0}, Segment {1}'.format(file_entry,seg_cnt))
                plt.axis([161.9e6,162.1e6,60,120])

                ais1 = 1.61975e8
                ais2 = 1.62025e8
                tfd_bw = 25000

                # Plot AIS Channel 2 BWs
                plt.vlines(ais1, 0, 125, colors='r', linestyles='--', linewidth=0.3)
                plt.vlines(ais1+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais1-480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais1 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
                plt.vlines(ais1 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)


                # Plot AIS Channel 2 BWs
                plt.vlines(ais2, 0, 125, colors='r', linestyles='--', linewidth=0.3)
                plt.vlines(ais2+480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais2 -480, 0, 125, colors='g', linestyles='--', linewidth=0.3)
                plt.vlines(ais2 + tfd_bw/2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
                plt.vlines(ais2 - tfd_bw / 2, 0, 125, colors='c', linestyles='--', linewidth=0.3)
                plt.grid(True)
                hold(False)
                plt.show(block=False)
                f_path, f_name = os.path.split(file_entry)
                save_dir = '/home/asi/Figures/'
                save_fn = f_name+'_'+str(seg_cnt)+'.png'

                if save_plot is True:
                    plt.savefig(save_dir+save_fn,format='png')
                    print('Saving {0}{1}'.format(save_dir, save_fn))
                plt.pause(0.75)
                seg_cnt +=1
        else:
            print('{0} tune frequency was {1}'.format(file_entry,cf))
    return

def convert(chandata, array_length, byte_flag):
    """
    Convert 32 bit complex values to 16 bit integer alternating I/Q Samples
    :param chandata: Data to convert
    :param array_length: Length of array to store converted values
    :param byte_flag: If True, output data as bytes
    :return: Byte array of 16 bit integer alternating I/Q Samples
    """
    # Normalize data
    l1 = abs(chandata.real).max()
    l2 = abs(chandata.imag).max()
    l = max(l1, l2)

    print('Input Max is {}'.format(l))
    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = np.int16(chandata.real * (((1 << 15)-1) / l / np.sqrt(2)))
    a[1::2] = np.int16(chandata.imag * (((1 << 15)-1) / l / np.sqrt(2)))

    mins = len(np.where(a==-32768))
    maxs = len(np.where(a==32767))
    print('Normailized Max a is {0}, Min a is {1}'.format(max(a), min(a)))
    print('Num normalized max vals : {0}, Num min vals : {1}'.format(maxs,mins))


    print('Chunk Size : {} '.format((a.size)))

    if byte_flag is True:
        return a.tobytes('C')
    elif byte_flag is False:
        return a


if __name__=='__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", help="Find npz files that match this string", type=str, default='*AIS*.npz')
    parser.add_argument("--dir", help="Directory to search", type=str, default='/run/media/asi/ASI_EXT2/')
    parser.add_argument("--channel", help="Channel to plot", type=int, default=4)
    parser.add_argument("--time_ms", help="Time in ms to FFT", type=int, default=100)
    parser.add_argument("--save_plot", help="Save Plot Output", type=bool, default=False)
    parser.add_argument("--convert", help="Convert from 32 bit complex IQ floats to 16 bit alternating IQ ints",
                        type=bool, default=False)
    args = parser.parse_args()

    print('Plotting {0}{1}'.format(args.dir,args.infile))

    if args.time_ms == 0:
        plot_psd_16(infile=args.infile, dir=args.dir, channel=args.channel)
    elif args.time_ms >= 1:
        plot_psd_time_segments(infile=args.infile, dir=args.dir, channel=args.channel, time_ms=args.time_ms,
                               save_plot=args.save_plot, conv_bytes=args.infile.convert)
    else:
        print('Invalid argument for --segments')