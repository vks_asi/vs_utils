import csv
import glob
import os
import operator
from CalculateSNR import CalculateSNR
from ast import literal_eval


def check_ais_high_snr():
    rs = filter_csv()


    for file in rs:


        of = 'ais_snr_filtered.csv'

        freqs = [161975000, 162025000]

        r_dict = {}
        r_dict['FILE'] = None
        r_dict['CF'] = None
        r_dict['BW'] = None
        r_dict['FS'] = None
        r_dict['FS_new'] = None
        r_dict['slice_time'] = None
        r_dict['record_time'] = None
        r_dict['SNR'] = None
        r_dict['num_pts'] = None

        with open(of, 'w') as outfile:
            fieldnames = r_dict.keys()
            writer = csv.DictWriter(outfile, fieldnames=fieldnames)
            writer.writeheader()

            for freq in freqs:
                for file in rs:
                    r_dict = {}
                    a = CalculateSNR()
                    a.load_data(file)
                    a.sig_freq = freq
                    a.sig_bw = 25000
                    a.slice_ms = 25
                    a.start_channel = 4
                    a.end_channel = 8
                    a.decimate()
                    a.decimate()
                    a.calc_snr()
                    r_dict['FILE'] = file
                    r_dict['CF'] = a.sig_freq
                    r_dict['BW'] = a.sig_bw
                    r_dict['FS'] = a._sample_rate
                    r_dict['FS_new'] = a.decimation_sr
                    r_dict['slice_time'] = a.slice_ms
                    r_dict['record_time'] = a.collection_time
                    r_dict['SNR'] = a.snr_max
                    r_dict['num_pts'] = a.pts_between

                    writer.writerow(r_dict)
                    print(r_dict)
    return


def check_ais_recordings():

    fn = 'ais.csv'

    of = 'ais_snr.csv'

    # Get .npz recordings
    test_data_path = '/run/media/asi/ASI_EXT2/*AIS_*.npz'
    ais_npz = glob.glob(test_data_path)
    ais_npz.sort(key=os.path.getmtime)

    # Get .dat recordings
    ais_dats = []
    with open(fn, 'r') as aisfile:
        reader = csv.reader(aisfile)
        next(reader, None)
        for row in reader:
            ais_dats.append(row[0])

    # Merge .npz and .dat file lists
    for x in ais_npz:
        ais_dats.append(x)

    print(len(ais_dats))
    freqs = [161975000, 162025000]

    r_dict = {}
    r_dict['FILE'] = None
    r_dict['CF'] = None
    r_dict['BW'] = None
    r_dict['FS'] = None
    r_dict['FS_new'] = None
    r_dict['slice_time'] = None
    r_dict['record_time'] = None
    r_dict['SNR'] = None

    with open(of, 'w') as outfile:
        fieldnames = r_dict.keys()
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
        writer.writeheader()

        for freq in freqs:
            for file in ais_dats:
                r_dict = {}
                a = CalculateSNR()
                a.load_data(file)
                a.sig_freq = freq
                a.sig_bw = 25000
                a.slice_ms = 25
                a.start_channel = 4
                a.end_channel = 8
                a.decimate()
                a.decimate()
                a.calc_snr()
                r_dict['FILE'] = file
                r_dict['CF'] = a.sig_freq
                r_dict['BW'] = a.sig_bw
                r_dict['FS'] = a._sample_rate
                r_dict['FS_new'] = a.decimation_sr
                r_dict['slice_time'] = a.slice_ms
                r_dict['record_time'] = a.collection_time
                r_dict['SNR'] = a.snr_max

                writer.writerow(r_dict)
                print(r_dict)

                return

def reformat_csv():

    fn = 'ais_snr.csv'
    of = 'ais_snr_reformatted.csv'

    results = []
    with open(fn,'r') as csvfo:
        reader = csv.DictReader(csvfo)
        for row in reader:
            row['SNR'] = literal_eval(row['SNR'][:])
            snr_keys = row['SNR'].keys()
            for key in snr_keys:
                row[key] = row['SNR'][key]

            row.move_to_end(key='SNR')
            row.popitem(last=True)
            results.append(row)
        csvfo.close()

    with open(of,'w') as outfo:
        writer = csv.DictWriter(outfo, fieldnames=results[0].keys())
        writer.writeheader()
        for result in results:
            writer.writerow(rowdict=result)
        outfo.close()

def filter_csv():
    fn = 'ais_snr_reformatted.csv'
    results = []

    with open(fn,'r') as csvfo:
        reader = csv.DictReader(csvfo)
        for row in reader:
            pwr = float(row['CH_5'])
            if pwr > 40.0:
                results.append(row['FILE'])
    print(len(results))
    results_set = set(results)
    print(len(results_set))
    return(results_set)

def get_ais_recordings():
    fn = 'ais_snr_filtered.csv'
    results = []

    with open(fn, 'r') as csvfo:
        reader = csv.DictReader(csvfo)
        for row in reader:
            row['SNR'] = literal_eval(row['SNR'])
            row['num_pts'] = literal_eval(row['num_pts'])
            results.append(row)
        csvfo.close()

    for x in results:
        x['snr_sum_pts'] = sum(x['num_pts'])
        x['snr_ave'] = (sum(x['SNR'].values())/len(x['SNR'].values()))

    results.sort(key=operator.itemgetter('snr_sum_pts'), reverse=True)

    high_snr = []
    for x in results:
        if x['snr_ave'] > 14.0:
            high_snr.append(x)

    high_snr.sort(key=operator.itemgetter('snr_sum_pts'), reverse=True)

    file_list = []
    for x in high_snr:
        file_list.append(x['FILE'])
    return file_list

if __name__=='__main__':
    fl = get_ais_recordings()
    for x in fl:
        print(x)