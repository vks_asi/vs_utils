#!/usr/bin/env python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import numpy as np
import os
from SignalProcessing.plot_overload import *
from AsiUtilities.data_file_io import read_mp
from functools import reduce
from scipy.signal import decimate


def shift_decimate(signal, fs=12.8e6, freq_shift=0, decimation_factor=10):
    """
    Shift the signal, then decimate using scipy.signal's decimate function
    :param signal: Input Signal (one channel) to shift and decimate
    :param fs: Sample Rate
    :param freq_shift: To shift the signal to the left by 500 kHz, freq_shift=0.5e6
    :param decimation_factor: decimation factor for scipy.signal decimate
    :return: signal
    """
    if freq_shift != 0:
        out_signal = signal*(np.exp(1j * 2 * np.pi * freq_shift * np.arange(signal.size[-1]) / fs))
    else:
        out_signal = signal.copy()
    out_signal = decimate(out_signal, decimation_factor)
    return out_signal


def get_recording_data(input_file):
    """
    Get data from an input_file created using savez or AsiUtilities.data_file_io read_mp
    :param input_file: File to load
    :return: data, nav, rs,
    """
    if '.npz' in input_file:
        loaded = np.load(input_file)
        data = loaded['tdd']
        nav = loaded['nav_data']
        rs = loaded['radio_state']
        rs = rs[()]
        nav = nav[()]
    elif '.dat' in input_file:
        data, nav, rs, _ = read_mp(input_file)
    else:
        data = None
        nav = None
        rs = None
    return data, nav, rs


class CalculateSNR(object):
    """
    CalculateSNR takes npz or dat recording files, loads IQ data, splits the data into slices, calculates the spectrum
    and SNR for each slice.

    Usage example located in main

    Member methods are as follows:
        load_data - loads data from a dat or npz file
        decimate - decimates and filters data using scipy.signal decimate
        calc_snr - reshapes data, calculates spectrum, frequency vector, and snr
        plot_spectrum - plot the spectrum of each slice
        plot_snr - plot the per slice snr
    """
    def __init__(self):

        # Radio State Parameters, loaded from recording file
        self.data = None
        self.radio_state = None
        self.nav = None
        self._sample_rate = None
        self.tune_freq = None
        self.tune_bw = None
        self.collection_time = None
        self.radio_model = None
        self.channels = []

        # Signal Processing Parameters, set by user
        self.start_channel = 4
        self.end_channel = 8
        self.decimation_factor = 10
        self.slice_ms = 25      # Slice time in milliseconds
        self.sig_bw = 25e3
        self.sig_freq = 161.975e6
        self.threshold_dB = 10.0

        # Result Holders
        self.snr = 0
        self.snr_db = 0
        self.snr_max = 0
        self.spectrum = None
        self.frequency_vector = None
        self.decimation_iterations = 0
        self.decimation_sr = 0
        self.act_slices = 0
        self.pts_between = 0

        # Plot Settings
        self.file = None
        self.plot_font = {'family': 'FreeSans','color': 'black','weight': 'normal','size': 10,}
        self.ms_per_slice = 0

    def load_data(self, file):
        """ Load data from a .dat or .npz file."""

        _,self.file = os.path.split(file)
        self.data, self.nav, self.radio_state = get_recording_data(file)

        self._sample_rate = self.radio_state['data_rate']
        self.tune_freq = self.radio_state['frequency'][0]
        self.tune_bw = self.radio_state['bandwidth']
        self.collection_time = self.radio_state['collect_time']
        try:
            num_channels = len(self.radio_state['frequency'])
        except ValueError:
            num_channels = 8
        try:
            self.radio_model = self.radio_state['model_number']
        except ValueError:
            self.radio_model = None
        self.channels = np.arange(0,num_channels,1)

    def decimate(self):
        """ Decimate self.data, update self.decimation_sr with the new sample rate."""
        sc = self.start_channel
        ec = self.end_channel
        if self.decimation_iterations is 0:
            self.data = shift_decimate(self.data[sc:ec], fs=self._sample_rate, freq_shift=0,
                                   decimation_factor=self.decimation_factor)
            self.decimation_sr = self._sample_rate/self.decimation_factor
        else:
            self.data = shift_decimate(self.data[0:], fs=self.decimation_sr, freq_shift=0,
                                       decimation_factor=self.decimation_factor)
            self.decimation_sr = self.decimation_sr/self.decimation_factor

        self.decimation_iterations += 1

    def calc_snr(self):
        """
        Calculates the per-channel snr of self.data by:
        1. Split self.data into slices are self.slice_ms long.
        2. Take the FFT of each slice, calculate associated frequency vector.
        3. Calculate average signal power in each slice.
        4. Calculate average noise power in each slice.
        """
        def factors(n):
            """ Return a sorted list of factors of n."""

            return sorted(set(reduce(list.__add__,([i, n // i] for i in range(1, int(n ** 0.5) + 1) if n % i == 0))))

        def find_nearest(array, value):
            """ Find element nearest to value in array. Return element. """
            y = np.array([],dtype=np.int64)
            for x in array:
                y = np.append(y,x)
            y = np.delete(y, np.where(y==1))
            idx = np.abs(y - value).argmin()
            return y[idx]

        def find_freq(freq_vector, frequency):
            """ Find closest element to frequency in freq_vector. Return location in freq_vector."""
            index = abs(freq_vector-frequency).argmin()
            return index


        def calc_power(f1_dx, f2_dx, spectrum):
            """
            Calculate average power in spectrum over band defined by f2_dx and f1_dx
            :param f1_dx: Signal start, index in spectrum
            :param f2_dx: Signal end, index in spectrum
            :param spectrum: Spectrum to calculate power
            :return: pwr, Mean signal power in band
            """
            if len(spectrum.shape) == 3:
                pwr = (abs(spectrum[:, :, f1_dx:f2_dx]) ** 2).mean(-1)
            elif len(spectrum.shape) == 2:
                pwr = (abs(spectrum[:, f1_dx:f2_dx]) ** 2).mean(-1)
            return pwr

        def find_max(snr):
            """ Find the max value of each channel. Return max value in a dictionary"""
            max_dict = {}
            max_keys = []
            channels = np.arange(self.start_channel,self.end_channel,1)
            for c in channels: max_keys.append('CH_{}'.format(c))

            columns = np.arange(0,snr.shape[0],1)
            for x,col in enumerate(columns):max_dict[max_keys[x]] = np.round(max(snr[col,:]),3)

            return max_dict

        def find_num_above(snr, threshold_dB, max_dict):
            """ Find number of snr points above threshold and below max"""
            chan_seg_cnts = []
            max_list = []
            for val in max_dict.values(): max_list.append(val)
            columns = np.arange(0,snr.shape[0],1)
            for x,col in enumerate(columns):
                sc = np.where((snr[col,:] > threshold_dB) & (snr[col,:] < max_list[x]))
                chan_seg_cnts.append(len(sc[0]))
            return chan_seg_cnts

        # Remove one element from each channel if number of samples is odd,
        if self.data.shape[1] % 2:
            self.data = self.data[:,:-1]
            print('Removed one element from each column, shape is now {}'.format(self.data.shape))

        # Estimate number of slices, and corresponding data length
        data_chans = self.data.shape[0]
        data_length = self.data.shape[1]
        reshape_length = np.int(self.slice_ms/1000 * self.decimation_sr)
        num_slices = np.int(data_length//reshape_length)

        # Find the closest value to the desired number of slices
        self.act_slices = find_nearest(factors(data_length), num_slices)
        reshape_length = np.int(data_length/self.act_slices)
        self.ms_per_slice = np.round((self.collection_time/self.act_slices) * 1000,2)

        # Reshape data into matrix with shape (channels, slices, samples)
        # Breaks each channel of data into act_slices each with data length of reshape_length
        self.data = self.data.reshape(data_chans, self.act_slices, reshape_length)

        self.spectrum = np.fft.fftshift(np.fft.fft(self.data), axes=-1)

        # Check to see if radio is NDR308, correct tune frequency if the tune frequency resolution is finer than MHz
        if self.radio_model in ['NDR308'] and self.tune_freq % 1e6 > 0:
            of = self.tune_freq
            self.tune_freq = np.int(self.tune_freq - (self.tune_freq%1e6))
            print('Radio is {0}, corrected to tune frequency from {1} to {2}'.format(self.radio_model, of, self.tune_freq))

        self.frequency_vector = np.arange(-self.spectrum.shape[-1]//2, self.spectrum.shape[-1]//2) * \
                                self.decimation_sr/self.spectrum.shape[-1]+self.tune_freq

        # Find the signal start and end points
        sig_start = find_freq(self.frequency_vector, self.sig_freq-self.sig_bw/2)
        sig_end = find_freq(self.frequency_vector, self.sig_freq+self.sig_bw/2)

        # Assert that the spectrum bandwidth is larger than the signal bandwidth
        spec_bw = self.frequency_vector[-1] - self.frequency_vector[0]
        assert ((self.frequency_vector[sig_end] - self.frequency_vector[sig_start]) < spec_bw), \
            'Signal bandwidth larger than Spectrum Bandwidth!'

        sig_pwr = calc_power(sig_start, sig_end, self.spectrum)

        noise_bw_one = self.frequency_vector[sig_start] - self.frequency_vector[0]
        noise_bw_two = self.frequency_vector[-1] - self.frequency_vector[sig_end]

        # Use the larger noise bandwidth to calculate noise
        if max(noise_bw_one, noise_bw_two) is noise_bw_one:
            n_pwr = calc_power(0, sig_start, self.spectrum)
            print('Noise Bandwidth is {}'.format(noise_bw_one))
        else:
            n_pwr = calc_power(sig_end, -1, self.spectrum)
            print('Noise Bandwidth is {}'.format(noise_bw_two))

        # Calculate the SNR
        self.snr = sig_pwr/n_pwr
        self.snr_db = 10*np.log10(self.snr)
        self.snr_max = find_max(self.snr_db)
        self.pts_between = find_num_above(self.snr_db, self.threshold_dB, self.snr_max)


    def plot_spectrum(self):
        """ Plot the spectrum of each slice."""

        hold(False)

        for m in range(self.act_slices):
            plot(self.frequency_vector,20*np.log10(abs(self.spectrum[:,m].T)))
            plt.suptitle(self.file,fontdict=self.plot_font)
            plt.title('{0} ms slice {1}'.format(self.ms_per_slice,m),fontdict=self.plot_font)
            plt.ylabel('Power, dB', fontdict=self.plot_font)
            plt.xlabel('Frequency, Hz', fontdict=self.plot_font)

            leg_label = []
            channels = np.arange(self.start_channel,self.end_channel,1)
            for c in channels: leg_label.append('CH{}'.format(c))

            plt.legend(leg_label)
            plt.pause(0.25)

    def plot_snr(self):
        """ Plot the per slice SNR of each channel."""
        hold(False)

        leg_label = []
        channels = np.arange(self.start_channel, self.end_channel, 1)
        for c in channels: leg_label.append('CH{}'.format(c))

        x_label = '{0} ms slices, fc = {1}, bw = {2}, fs = {3}'.format(self.ms_per_slice, self.sig_freq, self.sig_bw,
                                                                       self.decimation_sr)
        y_label = 'SNR, dB'

        plot(self.snr_db.T)
        plt.legend(leg_label)
        plt.title(self.file,fontdict=self.plot_font)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.show()


if __name__=='__main__':

    a = CalculateSNR()
    #a.load_data('/run/media/asi/ASI_EXT1/CALCOFI_9345_264/CALCOFI_9345_264-nb-2018-10-15_10-00-06-076419.dat')
    #a.load_data('/run/media/asi/ASI_EXT2/CALCOFI_8355_12HR2018-10-24_16-46-58-534082CALCOFI_8355_12HR_AIS_162000000.0.npz')
    a.load_data('/run/media/asi/ASI_EXT1/CALCOFI_SCCOOS_underway/CALCOFI_SCCOOS_underway-nb-2018-10-20_13-55-20-839654.dat')
    print(a.radio_state)
    a.sig_freq=162025000
    a.sig_bw=25000
    a.slice_ms = 25
    a.start_channel = 4
    a.end_channel = 8
    a.decimate()
    a.decimate()
    a.calc_snr()
    print(a.decimation_sr, a.decimation_iterations)
    print(a.snr_max)
    print(a.pts_between)
    a.plot_spectrum()
    a.plot_snr()

