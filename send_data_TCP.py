def send_data_from_radio_TCP():
    nav = Navigation()
    radio = AsiNdr308InterfaceApi()
    radio.configure_wideband_ddc_group(data_rate=radio.wideband_filter_index['12.8 Msps'])
    print('Radio Sample Rate Set')
    radio.set_collection_time(0.65536)
    print('Radio Collection Time Set')
    radio.set_frequency(162e6)
    print('Radio Frequency Set')
    radio.get_iq_data()
    ndata = nav.get_nav()
    tdd = radio.get_time_domain_data()
    # print(radio.data[4].size)
    # print(radio.data[4].itemsize)
    # print(type(radio.data[4]))
    # print(radio.data[4].size*radio.data[4].itemsize)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket Created')
    s.connect((DEST_IP,DEST_PORT))
    print('Socket Connected')
    pc = 0
    stop = False
    try:
        while stop is False:
            print('{}'.format(radio.radio_parameters))
            print('Getting I/Q Data')
            radio.get_iq_data()
            for x in range(4,8,1):
                print('Sending Channel : {}'.format(x))
                num_split = radio.data[x].size/256
                split_data = np.split(radio.data[x], num_split)
                for dd in split_data:
                    dd = dd.astype(dtype=np.csingle)            # Convert to single type complex representation,
                                                                # Redhawk AIS implemenation expects 2 bytes, not four
                    s.send(dd.tobytes())
                    pc +=1
            #write_mp(deepcopy(tdd), deepcopy(ndata), radio_state=deepcopy(radio.get_radio_state()),
                     #data_type=DataType.NarrowBand, path=DATA_PATH, FILENAME_GPS_TIME=False,
                     #auto_basename=BASENAME)
            print('{} Packets Sent'.format(pc))
    except KeyboardInterrupt:
        stop = True
        print('Exiting')
    s.shutdown()
    s.close()
    print('Socket Closed')

    return