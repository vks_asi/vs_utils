
import hashlib
import json
import logging
import os
import sys
import traceback
import msgpack
import msgpack_numpy
from enum import Enum


_logger_DFIO = logging.getLogger(name='data_file_io')


class DataType(Enum):
    Raw = 0
    NarrowBand = 15000
    DDC2ndStage = 80000
    DDC1stStage = 800000
    Wideband = 8000000
    FortyMeg = 40000000


DataTypeAbbr = {
    DataType.Raw: 'raw',
    DataType.NarrowBand: 'nb',
    DataType.DDC2ndStage: 'ddc2',
    DataType.DDC1stStage: 'ddc1',
    DataType.Wideband: 'wb',
    DataType.FortyMeg: 'm40',
}



def read_mp(filename):
    """

    :param filename:
    :type filename: str
    :return: data, data_nav, radio_state_nav
    :rtype: np.multiarray.ndarray | None, dict | None, dict | None, DataType | None
    """

    # TODO: Hackish solution to string/bytes Python2/3 issues
    # the below works if the file was created with use_bin_type=True
    # unpacker = msgpack.Unpacker(stream, object_hook=msgpack_numpy.decode, encoding='utf-8')
    try:
        with open(filename, 'rb') as stream:
            unpacker = msgpack.Unpacker(stream, encoding='utf-8', object_hook=msgpack_numpy.decode)
            unpacked_list = []
            try:
                for unpacked in unpacker:
                    unpacked_list.append(unpacked)
            except UnicodeDecodeError:
                stream.seek(0)
                count = 0
                unpacker = msgpack.Unpacker(stream, object_hook=msgpack_numpy.decode)
                for unpacked in unpacker:
                    count += 1
                    if count > len(unpacked_list):
                        unpacked_list.append(unpacked)

            # Yet another string/unicode hack!!
            if isinstance(unpacked_list[3], dict):
                stream.seek(0)
                count = 0
                unpacker = msgpack.Unpacker(stream, object_hook=msgpack_numpy.decode)
                for unpacked in unpacker:
                    count += 1
                    if count == 4:
                        unpacked_list[3] = unpacked

        if len(unpacked_list) == 0:
            print(filename)
            raise ValueError('Bah')

        header = unpacked_list[0]
        """:type: dict"""
        data_radio_state = unpacked_list[1]
        """:type: dict"""
        data_nav = unpacked_list[2]
        """:type: dict"""
        data = unpacked_list[3]

        """:type: np.multiarray.ndarray"""
    except (TypeError, ValueError, IOError) as e:
        _logger_DFIO.warning("Read filename={} -> {}: {}".format(filename, type(e).__name__, e))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            _logger_DFIO.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        return None, None, None, None
    except Exception as e:
        _logger_DFIO.warning("Read filename={} -> {}: {}".format(filename, type(e).__name__, e))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            _logger_DFIO.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        return None, None, None, None

    # Validate Read
    try:
        header['data_type'] = DataType(header['data_type'])
        data_check = header['data_md5'] == hashlib.md5(data).hexdigest()
        data_radio_state_check = header['data_radio_state_md5'] == hashlib.md5(
            json.dumps(data_radio_state, sort_keys=True).encode()).hexdigest()
        data_nav_check = header['data_nav_md5'] == hashlib.md5(
            json.dumps(data_nav, sort_keys=True).encode()).hexdigest()
        # data_radio_state_valid = data_validator('data_radio_state', data_radio_state, header['version']) is not None
        data_radio_state_valid = True
        # data_nav_valid = data_validator('data_nav', data_nav, header['version']) is not None
        data_nav_valid = True

        if 'df_frequency' not in data_radio_state:
            data_radio_state['df_frequency'] = data_radio_state['frequency'][0]

        if data_check and data_radio_state_check and data_nav_check and data_radio_state_valid and data_nav_valid:
            _logger_DFIO.info("Read:{}".format(filename))
            return data, data_nav, data_radio_state, header['data_type']
        else:
            if not data_check:
                _logger_DFIO.warning('data MD5 mismatch for file: {}'.format(filename))
            if not data_radio_state_check or not data_nav_check:
                _logger_DFIO.warning('data properties MD5 mismatch for file {}'.format(filename))
            if not data_radio_state_valid or not data_nav_valid:
                _logger_DFIO.warning('data properties failed validation for file {}'.format(filename))
            return None, None, None, None
    except Exception as err:
        _logger_DFIO.warning("read_mp() -> {}:{}".format(type(err).__name__, err))
        for err_data in traceback.extract_tb(sys.exc_info()[2], limit=3):
            filename, line_no, function_name, line_text = err_data
            _logger_DFIO.debug("{}:{} in {}".format(os.path.basename(filename), line_no, function_name))
        _logger_DFIO.warning("read_mp() Failed to Read File: {}".format(filename))
        return None, None, None, None


if __name__=='__main__':
    file = '/run/media/asi/ASI_EXT1/CALCOFI_9345_264/CALCOFI_9345_264-nb-2018-10-15_10-00-11-618204.dat'
    data,nav,radio_state,_ = read_mp(file)
    print(data.shape)
    print(nav)
    print(radio_state)
