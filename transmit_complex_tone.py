import socket
import numpy as np
from time import sleep

class TransmitComplexTone():
    """
        Class that transmits a complex tone through a socket

    """
    def __init__(self):
        self.buffer = 4096
        self.samples = 1024
        self.tx_port = 50010
        self.tx_ip = '127.0.0.1'
        self.fs = 48000
        self.cf = 1000
        self.test = False

    def run(self):

        tx_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        tx_sock.bind((self.tx_ip,0))


        if self.test:
            rx_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            rx_sock.bind((self.tx_ip,self.tx_port))

        cnt = 0
        data_out = np.zeros(self.samples*2, np.uint16)

        try:
            while 1:
                data = np.complex64(np.exp(1j*2*np.pi*self.cf*np.arange(cnt,cnt+self.samples)/self.fs))
                data_out[::2] = np.uint16(data.real*128)
                data_out[1::2] = np.uint16(data.imag*128)
                tx_sock.sendto(data_out.tobytes('C'),(self.tx_ip,self.tx_port))
                sleep(.02)
                if self.test:
                    d = rx_sock.recv(4096)
                    print(len(d))
                cnt += self.samples

        except (KeyboardInterrupt, SystemExit):
            tx_sock.close()


if __name__=='__main__':

    rx = TransmitComplexTone()
    rx.run()
