import os
import numpy as np
from scipy.io import wavfile
from AsiUtilities.data_file_io import read_mp
from send_data import convert, shift_decimate
from CalculateSNR import get_recording_data
from process_ais import get_ais_recordings

def convert_to_wav(input_file, out_dir):

    if not out_dir:
        out_dir = '/home/asi/test_data/'

    data, nav, rs = get_recording_data(input_file)

    #convert_data = in_data['tdd']
    chan5 = data[5]
    print('Data shape : {}'.format(chan5.shape))
    fs = 12.8e6
    shift = 0
    df = 10

    chan5_sd = shift_decimate(chan5, fs=fs, freq_shift=shift, decimation_factor=df)
    fs = int(fs // df)

    chan5_sd_shorts = convert(chan5_sd, len(chan5_sd), byte_flag=False)
    print(chan5_sd_shorts.size)
    print(chan5_sd_shorts.dtype)

    real = chan5_sd_shorts[0::2]
    imag = chan5_sd_shorts[1::2]

    real_imag = np.vstack((real,imag)).T
    print(real_imag.shape)

    path, name = os.path.split(input_file)
    if shift != 0:
        oname = name+'_{0}sps_{1}Hzshift.wav'.format(fs, shift)
    else:
        oname = name + '_{}sps.wav'.format(fs)

    wavfile.write(out_dir+oname,fs,real_imag)
    print('Created {}'.format(out_dir+oname))
    return

if __name__=='__main__':
    out_dir = '/home/asi/test_data/wavs/'
    file_list = get_ais_recordings()
    for file in file_list:
        convert_to_wav(file, out_dir)
    print('Completed Conversion')

