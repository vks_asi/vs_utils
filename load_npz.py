import numpy as np
import glob
import os
import time
import matplotlib.pyplot as plt
import matplotlib.pylab as mth

def check_ais(data):
    threshold = 9.0
    samples = 1000
    data_index = data[np.where(data > threshold)]
    if len(data_index) >= samples:
        print(len(data_index))
        return True
    else:
        return False

# Define input file
test_data_path = '/run/media/asi/ASI_EXT2/CALCOFI*AIS*.npz'

file_list = glob.glob(test_data_path)
file_list.sort(key=os.path.getmtime)
print(file_list)
for file in file_list:
    try:
        # Load data from file
        loaded = np.load(file)

        # Get time domain data
        time_domain_data = loaded['tdd']

        # Get time domain data for radio channels 1-4, which were connected  from S-band antenna to NDR308
        # Get real component of each sample, transpose
        # channel_5 = time_domain_data[4].real.T
        # channel_6 = time_domain_data[5].real.T
        # channel_7 = time_domain_data[6].real.T
        # channel_8 = time_domain_data[7].real.T

        ais_channels = [4,5,6,7]

        chan_status = []
        for channel in ais_channels:
            result = check_ais(time_domain_data[channel].real.T)
            chan_status.append(result)

        if True in chan_status:
            print(file, chan_status)
            chan_status = []
        else:
            print('No AIS found in {}'.format(file))
            chan_status = []


        # Get navigation data
        #navigation_data = loaded['nav_data']

        # Get Radio State
        #radio_state = loaded['radio_state']


        # Create a range to represent time
        #t = np.arange(0, len(channel_5))

        # Plot Channel 1 Power vs. Time
        # plt.plot(t,20*mth.log10(abs(channel_1)))
        # plt.plot(t,channel_5)
        # plt.plot(t,channel_6)
        # plt.plot(t,channel_7)
        # plt.plot(t,channel_8)
        # plt.title(file[26:])
        # print(file)
        # plt.show()
        # plt.close()
    except KeyboardInterrupt:
        break



