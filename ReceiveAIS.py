import socket
from time import sleep

class ReceiveAIS():

    def __init__(self):

        self.ip_addr = '127.0.0.1'
        self.port = 32191
        self.buffer = 4096


        self.tx_port = 50010
        self.tx_ip = '127.0.0.1'

    def run(self):

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((self.ip_addr, self.port))
        s.listen(1)
        conn, addr = s.accept()

        tx_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        tx_sock.bind((self.ip_addr,0))

        while 1:
            data = conn.recv(self.buffer)

            if not data:
                break
            tx_sock.sendto(data,(self.tx_ip,self.tx_port))
            sleep(.02)
        conn.close()
        tx_sock.close()


if __name__=='__main__':

    rx = ReceiveAIS()
    rx.run()

